const express = require("express");
const app = express();
const axios = require("./axios.config");
const { validUrl } = require('./allowed-url.json')
const { AMBIENTE } = process.env;


app.post("/", async (req, res) => {
  try {
    const { username, password, redirectTo } = req.body;

	console.log(req.body)
	const oURL = new URL(redirectTo);
	if (!validUrl.includes(oURL.origin))  throw new Error('La url destino no es valida')
	
    const response = await axios.post("/AUTH_AUTHENTICATE/" + AMBIENTE, {username, password});
    const token = response.data.jwt;

    let fechaExpiracion = new Date();
    fechaExpiracion.setSeconds(fechaExpiracion.getSeconds() + 10);
	oURL.searchParams.append('_k', token)

    let resp = {
      fechaExpiracion,
      status: "Credenciales correctas",
      usuario: req.body.username,
      token,
	  url: oURL.href

    };

	res.json(resp);

} catch (error) {
	let status = error.status || 500;
	if(status == 500) console.error(error)
    let mensaje = error?.response?.data || { msg: "server error" };
	if(error instanceof Error) {
		mensaje = error.message
		status = 401
	}
    res.status(status).json(mensaje);
  }
});

app.get("/isLogin", async (req, res) => {
  try {
    const tokenAvalidar = req.get("Authorization");
    const response = await axios.post("/AUTH_REFRESH_TOKEN/" + AMBIENTE, { jwt: tokenAvalidar });
    const token = response.data.jwt;
    let fechaExpiracion = new Date();
    fechaExpiracion.setHours(fechaExpiracion.getHours() + 4);
    let resp = {
      fechaExpiracion,
      status: "Credenciales correctas",
      usuario: req.body.username,
      token,
    };
    res.setHeader("Access-Control-Expose-Headers", "*");
    res.setHeader("Authorization", token);
    res.json(resp);
  } catch (error) {
    let status = error.status || 500;
    let mensaje = error?.response?.data || { msg: "server error" };
    res.status(status).json(mensaje);
  }
});

module.exports = app;
