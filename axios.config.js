const https = require('https');
const fs = require('fs')
const axios = require('axios').default

/**
 * get AxiosConfig
 *
 * @return {import('axios').AxiosRequestConfig<any>} 
 */
const httpsAgent = new https.Agent({
    cert: fs.readFileSync(process.env.CRT_XROAD),
    key: fs.readFileSync(process.env.KEY_SSL_XROAD),
    rejectUnauthorized: false,
});

const axiosIntance = axios.create({
    baseURL: process.env.XROAD_BASEURL,
    httpsAgent,
    headers: { 
        'X-Road-Client': process.env.XROAD_CLIENT_OPTIC,
        'Content-Type':'application/json'},
})
module.exports = axiosIntance