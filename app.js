const express = require('express');
const cors = require('cors');
const app = express();

require('dotenv').config()

app.use(express.json());
app.use(cors())
app.use(express.urlencoded({ extended: false}));

app.use("/login", require("./login.controller"));

app.listen(process.env.PORT, () => console.log("Server Running on " + process.env.PORT));

